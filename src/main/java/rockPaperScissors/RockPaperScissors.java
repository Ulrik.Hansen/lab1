package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        while (true)
        {
            System.out.println("Let's play round "+roundCounter);
            roundCounter++;
            while (true) {
                String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?");
                if (!rpsChoices.contains(humanChoice)) {
                    System.out.println("I don't understand "+humanChoice+". Try again");
                    continue;
                }    
                else {
                    Random rand = new Random();
                    String computerChoice = rpsChoices.get(rand.nextInt(rpsChoices.size()));
                    if (winner(humanChoice,computerChoice)) {
                        humanScore++;
                        announce("Human",humanChoice,computerChoice);
                    }
                    else if (winner(computerChoice,humanChoice)) {
                        computerScore++;
                        announce("Computer",humanChoice,computerChoice);
                    }
                    else {
                        System.out.println("Human chose "+humanChoice+", computer chose "+computerChoice+". It's a tie!");
                    }
                    break;
                }
                
            }
            System.out.println("Score: human "+humanScore+", computer "+computerScore);
            String exitCheck = readInput("Do you wish to continue playing? (y/n)?");
            if (exitCheck.equals("y")) {
                continue;
            }
            else {
                System.out.println("Bye bye :)");
                break;
            }
        }

    }

    public boolean winner(String choice1, String choice2) {
        if (choice1.equals("rock")) {
            return (choice2.equals("scissors"));
        }
        else if (choice1.equals("paper")) {
            return (choice2.equals("rock"));
        }
        else if (choice1.equals("scissors")) {
            return (choice2.equals("paper"));
        }
        else {
            return false;
        }
    }
    public void announce(String winner, String choice1, String choice2) {
        System.out.println("Human chose "+choice1+", computer chose "+choice2+". "+winner+" wins!");
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
